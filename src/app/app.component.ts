import { Component } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string;
  isHomePage: boolean = false;
  getUrl: string = 'Simulador';

  constructor (
    private router: Router,

  ) {
    this.router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        this.getUrl = event.url;

        if (this.getUrl == '/') {
          this.isHomePage = true;
          this.title = 'Simulador';
        }
        if (this.getUrl != '/') {
          this.isHomePage = false;
          this.title = this.getUrl == '/new-proposal' ? 'Nova proposta' : 'Propostas aceitas';
        }
      }
    });
  }

  ngOnInit() {
  }
}
