import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcceptedProposalsComponent } from './accepted-proposals/accepted-proposals.component';
import { HomePageComponent } from './home-page/home-page.component';
import { NewProposalComponent } from './new-proposal/new-proposal.component';


const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'new-proposal', component: NewProposalComponent},
  {path: 'accepted-proposals', component: AcceptedProposalsComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
