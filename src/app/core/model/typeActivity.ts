export interface TypeActivity {
  id: string;
  name: string;
  minRateDebit: number;
  minRateCredit: number;
}
