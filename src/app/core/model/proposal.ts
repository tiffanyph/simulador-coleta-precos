export interface Proposal {
  document: string;
  phoneNumber: string;
  email?: string;
  idTypeActivity: string;
  idConcurrent: string;
  debitRateConcurrent: string;
  creditRateConcurrent: string;
  debitDiscountOffered: string;
  creditDiscountOffered: string;
  debitDiscountAccepted: string;
  creditDiscountAccepted: string;
  accepted: boolean;
  proposalAcceptedAt: string;
}
