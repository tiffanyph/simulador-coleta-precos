import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CustomValidatorService {

  constructor() { }

  static notBeBlank(control: FormControl) {
    if (!control.value) {
      return null;
    }
    const stringReceived = control.value;
    if (!stringReceived || stringReceived.toString().trim().length < 2) {
      return { empty_field: true };
    }
    return null;
  }

  static onlyNumbers(control: FormControl) {
    const controlNumber = control.value;
    const controlNumberRegExp = /^[0-9]*$/;
    if (controlNumber) {
      return controlNumberRegExp.test(controlNumber) ? null : { invalid_control_number: true };
    }
    return null;
  }
}
