import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-accepted-proposal-dialog',
  templateUrl: './accepted-proposal-dialog.component.html',
  styleUrls: ['./accepted-proposal-dialog.component.scss']
})
export class AcceptedProposalDialog implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<AcceptedProposalDialog>
  ) {}

  ngOnInit() {

  }

  close() {
    this.dialogRef.close(1)
  }
}
