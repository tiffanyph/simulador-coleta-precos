import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Proposal } from '../core/model/proposal';
import { CustomValidatorService } from '../core/services/validator/custom-validator.service';
import {MatDialog} from '@angular/material/dialog';

import { Concurrent } from '../core/model/concurrent';
import { TypeActivity } from '../core/model/typeActivity';
import { MatTableDataSource } from '@angular/material/table';
import { AcceptedProposalDialog } from '../core/dialog/accepted-proposal-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-proposal',
  templateUrl: './new-proposal.component.html',
  styleUrls: ['./new-proposal.component.scss']
})

export class NewProposalComponent implements OnInit {
  dataFirstStepForm: FormGroup;
  dataSecondStepForm: FormGroup;

  documentMask: string = '';
  messageProposal: string = '';
  errorFirstForm: boolean = false;
  errorSecondForm: boolean = false;
  debitIsValid: boolean = false;
  creditIsValid: boolean = false;
  dataFirstStep = {} as any;
  dataSecondStep = {} as any;
  newProposal = {} as any;

  listTable: any[] = [
    { type: 'Débito', concurrentRate: 0, discountOffered: 0 },
    { type: 'Crédito', concurrentRate: 0, discountOffered: 0 },
  ];

  dataSource: any;
  displayedColumns: string[] = ['type', 'concurrentRate', 'discountOffered'];

  typeActivities: TypeActivity[] = [
    { id: 'TAID1', name: 'Atividade 1', minRateDebit: 4.5, minRateCredit: 2.5 },
    { id: 'TAID2', name: 'Atividade 2', minRateDebit: 2.0, minRateCredit: 2.1 },
    { id: 'TAID3', name: 'Atividade 3', minRateDebit: 3.2, minRateCredit: 1.5 },
    { id: 'TAID4', name: 'Atividade 4', minRateDebit: 1.9, minRateCredit: 2.3 },
    { id: 'TAID5', name: 'Atividade 5', minRateDebit: 2.6, minRateCredit: 2.3 },
    { id: 'TAID6', name: 'Atividade 6', minRateDebit: 2.1, minRateCredit: 1.9 },
  ];
  concurrentList: Concurrent[] = [
    { id: 'CLID1', name: 'Concorrente 1' },
    { id: 'CLID2', name: 'Concorrente 2' },
    { id: 'CLID3', name: 'Concorrente 3' },
    { id: 'CLID4', name: 'Concorrente 4' },
    { id: 'CLID5', name: 'Concorrente 5' },
    { id: 'CLID6', name: 'Concorrente 6' },
  ];

  constructor(
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  async ngOnInit() {
    this.dataFirstStepFormInit();
    this.dataSecondStepFormInit();
    this.loadDataTable();
  }

  loadDataTable() {
    this.dataSource = new MatTableDataSource(this.listTable);
  }

  dataFirstStepFormInit() {
    this.dataFirstStepForm = this.formBuilder.group({
      document: [null, [
        Validators.required,
        CustomValidatorService.notBeBlank
      ]],
      phoneNumber: [null, [
        Validators.maxLength(11),
        Validators.minLength(10),
        CustomValidatorService.notBeBlank
      ]],
      email: [null, [
        Validators.email
      ]],
      type: [null, [
        Validators.required
      ]]
    });
  }

  maskCPF(number) {
    let mask;
    mask = number.replace(/\D/g, '');

    if (mask.length < 11 || mask.length > 11 && mask.length < 14) return this.dataFirstStepForm.controls.document.setErrors({ min_length_invalid: true });

    if (number) this.documentMask = mask.length == 11 ? '000.000.000-00' : '00.000.000/0000-00';
  }

  clear() {
    this.documentMask = '';
  }

  submitFirstStep() {
    if (this.dataFirstStepForm.status === 'INVALID') {
      this.errorFirstForm = true;
      return;
    }
    const idActivity = this.typeActivities.find(item => this.dataFirstStepForm.controls.type.value == item.name).id;

    this.dataFirstStep = {
      document: this.dataFirstStepForm.controls.document.value,
      phoneNumber: this.dataFirstStepForm.controls.phoneNumber.value,
      email: this.dataFirstStepForm.controls.email.value,
      typeActivity: idActivity
    };

    this.newProposal = {
      ...this.dataFirstStep
    }
  }

  dataSecondStepFormInit() {
    this.dataSecondStepForm = this.formBuilder.group({
      concurrent: [null, [
        Validators.required,
      ]],
      debitRateConcurrent: [null, [
        Validators.required
      ]],
      debitDiscountOffered: [null, [
        Validators.required
      ]],
      creditRateConcurrent: [null, [
        Validators.required
      ]],
      creditDiscountOffered: [null, [
        Validators.required
      ]],
    });
  }

  submitSecondStep() {
    if (this.dataSecondStepForm.status === 'INVALID') {
      this.errorSecondForm = true;
      return;
    }
    const idConcurrent = this.concurrentList.find(item => this.dataSecondStepForm.controls.concurrent.value == item.name).id;
    const values = {
      concurrent: idConcurrent,
      debitRateConcurrent: this.dataSecondStepForm.controls.debitRateConcurrent.value,
      debitDiscountOffered: this.dataSecondStepForm.controls.debitDiscountOffered.value,
      creditRateConcurrent: this.dataSecondStepForm.controls.creditRateConcurrent.value,
      creditDiscountOffered: this.dataSecondStepForm.controls.creditDiscountOffered.value,
    }

    if (this.dataSecondStep.concurrent) {
      this.dataSecondStep.debitDiscountOffered = values.debitDiscountOffered;
      this.dataSecondStep.creditDiscountOffered = values.creditDiscountOffered;

    } else {
      this.dataSecondStep = values;
    }

    this.newProposal = {
      ...this.newProposal,
      ...this.dataSecondStep
    }

    this.verifyProposal();
  }

  verifyProposal() {
    const activity = this.dataFirstStep.typeActivity;
    const item = this.typeActivities.find(item => activity == item.id);

    if (Number(item?.minRateDebit) > Number(this.dataSecondStep.debitDiscountOffered)) {
      this.debitIsValid = false;
    }
    if (Number(item?.minRateDebit) <= Number(this.dataSecondStep.debitDiscountOffered)) {
      this.debitIsValid = true;
    }

    if (Number(item?.minRateCredit > Number(this.dataSecondStep.creditDiscountOffered))) {
      this.creditIsValid = false;
    } if (Number(item?.minRateCredit <= Number(this.dataSecondStep.creditDiscountOffered))) {
      this.creditIsValid = true;
    }
  }

  classTable(item) {
    if (item == 'Débito' && this.debitIsValid == false) {
      return 'invalid';
    }
    if (item == 'Crédito' && this.creditIsValid == false) {
      return 'invalid';
    }
    return 'valid';
  }

  getMessage() {
    const activity = this.dataFirstStep.typeActivity;
    const item = this.typeActivities.find(item => activity == item.id);

    if (this.debitIsValid == false || this.creditIsValid == false) {
      return `O valor de taxa mínima para débito é ${item?.minRateDebit}%, e para crédito é ${item?.minRateCredit}%.`;
    }

  }
  acceptedProposal() {
    const dialog = this.dialog.open(AcceptedProposalDialog);
    dialog.afterClosed().subscribe(response => {
      if (response) {
        this.router.navigate([`/`]).then();
      }
    })
  }

  valueOffered(item) {
    if (item == 'Débito') {
      return this.dataSecondStep.debitDiscountOffered;
    }
    if (item == 'Crédito') {
      return this.dataSecondStep.creditDiscountOffered;
    }
    return 'N/A';
  }

  rate(item) {
    if (item == 'Débito') {
      return this.dataSecondStep.debitRateConcurrent;
    }
    if (item == 'Crédito') {
      return this.dataSecondStep.creditRateConcurrent;
    }
    return 'N/A';
  }
}
