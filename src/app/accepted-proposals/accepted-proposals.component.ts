import { Component, OnInit } from '@angular/core';
import { Proposal } from '../core/model/proposal';

@Component({
  selector: 'app-accepted-proposals',
  templateUrl: './accepted-proposals.component.html',
  styleUrls: ['./accepted-proposals.component.scss']
})
export class AcceptedProposalsComponent implements OnInit {

  proposalList: Proposal[] = [
    {
      document: '46.384.813/0001-23', phoneNumber: '(17) 2922-2775', email: '', idTypeActivity: 'TAID1', idConcurrent: 'CLID5', debitRateConcurrent: '3.5%', creditRateConcurrent: '3.9%', debitDiscountOffered: '2.9%', creditDiscountOffered: '3.1%', debitDiscountAccepted: '2.6%', creditDiscountAccepted: '3.1%', accepted: true, proposalAcceptedAt: 'Mon Nov 12 2020'
    },
    {
      document: '062.980.537-78', phoneNumber: '(27) 3829-4169', email: 'empresa@email.com', idTypeActivity: 'TAID4', idConcurrent: '', debitRateConcurrent: '3.5%', creditRateConcurrent: '3.9%', debitDiscountOffered: '2.9%', creditDiscountOffered: '3.1%', debitDiscountAccepted: '2.6%', creditDiscountAccepted: '3.1%', accepted: false, proposalAcceptedAt: 'Mon Nov 12 2020'
    },
    {
      document: '51.483.776/0001-40', phoneNumber: '(27) 9 8759-8612', email: 'rh@davieandrelavanderiame.com.br', idTypeActivity: 'TAID3', idConcurrent: 'CLID5', debitRateConcurrent: '3.5%', creditRateConcurrent: '3.9%', debitDiscountOffered: '2.9%', creditDiscountOffered: '3.1%', debitDiscountAccepted: '2.6%', creditDiscountAccepted: '3.1%', accepted: true, proposalAcceptedAt: 'Mon Nov 12 2020'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

  convertToCSV(item) {
    const array = typeof item != 'object' ? JSON.parse(item) : item;
    let value = '';
    let line = '';

    for (let index in array) {
      if (line != '' && index != 'email' && array[index] != '' && index != 'accepted') line += ', ';
      if (line != '' && index == 'email' && array[index] != '') line += ', ';
      if (index != 'accepted') line += array[index];

    }
    value += line + '\r\n';
    return value;
  }
}
