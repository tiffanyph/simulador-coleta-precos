# simulador-coleta-precos

Sistema simulador para coletar os preços que um cliente possui no concorrente e oferecer um desconto melhor conforme o ramo de atividade dele. 

Instruções para rodar o projeto: 
- Ter o Angular configurado na máquina
- Clonar o projeto localmente na branch `master`
- Abrir o terminal na pasta do projeto e rodar `npm i` para instalar todas as dependências do projeto
- Após criada a pasta `node_modules` e arquivos de configuração, rodar o comando `npm start` no terminal
- Após aparecer a mensagem `Compiled successfully`, abrir no navegador o link http://localhost:4200/ e visualizar no modo _inspect_. Para isso, você clica com o botão direito do mouse em qualquer lugar da tela e seleciona a opção _Inspecionar_
na linguagem configurada na sua máquina.